package exemplogitlab;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.ufba.exemplogitlab.Soma;

public class SomaTest {
	
	@Test
	public void testZero() {
		assertEquals(0, Soma.soma(0, 0));
	}

	@Test
	public void testNegativos() {
		assertEquals(-3, Soma.soma(-1, -2));
	}


}
